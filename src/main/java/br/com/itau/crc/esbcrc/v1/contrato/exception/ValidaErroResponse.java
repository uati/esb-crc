package br.com.itau.crc.esbcrc.v1.contrato.exception;

import com.github.tomakehurst.wiremock.common.Json;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.validation.ConstraintViolation;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ValidaErroResponse {

    public void handlerErrorResponse(Exchange exchange) {
        BeanValidationException exception = (BeanValidationException) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
        Set<ConstraintViolation<Object>> constraintViolations = exception.getConstraintViolations();

        Map<Object, String> map =
                constraintViolations.stream()
                        .collect(Collectors.toMap(ConstraintViolation::getPropertyPath,
                                ConstraintViolation::getMessage));

        Message message = exchange.getOut();
        message.setHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        message.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.BAD_REQUEST.value());
        message.setBody(Json.write(map));
        message.setFault(false);
    }

}
