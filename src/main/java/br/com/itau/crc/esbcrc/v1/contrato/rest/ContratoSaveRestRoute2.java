package br.com.itau.crc.esbcrc.v1.contrato.rest;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import br.com.itau.crc.esbcrc.v1.contrato.exception.ValidaErroResponse;
import br.com.itau.crc.esbcrc.v1.contrato.route.ContratoSaveOrquestracaoRoute;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

@Component
public class ContratoSaveRestRoute2 extends RouteBuilder {

    public static final String REST_POST_CONTRATOS = "/api/contratos";
    public static final String PATH_CONTRATOS = "";
    public static final String DIRECT_REST_ROUTE_POST_CONTRATOS = "direct:REST_ROUTE_POST_CONTRATOS";
    public static final String ID_REST_ROUTE_POST_CONTRATOS = "ID_REST_ROUTE_POST_CONTRATOS";

    @Override
    public void configure() throws Exception {

        onException(BeanValidationException.class).handled(true).bean(ValidaErroResponse.class);

            rest(REST_POST_CONTRATOS)
                .post(PATH_CONTRATOS)
                .produces("application/json")
                .consumes("application/json")
                .type(Contrato.class)
                .route()
                    .from(DIRECT_REST_ROUTE_POST_CONTRATOS)
                    .routeId(ID_REST_ROUTE_POST_CONTRATOS)
                    //.log("${body}")
                    .unmarshal().json(JsonLibrary.Jackson, Contrato.class)
                    .process(exchange -> {
                        exchange.getOut().setBody(exchange.getIn().getBody(Contrato.class));
                    })
                    .to("bean-validator://x")
                    .to(ContratoSaveOrquestracaoRoute.DIRECT_ROUTE_ORQUESTACAO_SAVE)
                    //.log("${body}")
                    .marshal().json(JsonLibrary.Jackson)
                    //.setHeader(Exchange.HTTP_RESPONSE_CODE, simple("${in.header.CamelHttpResponseCode}"))
                    .endRest();
                    

    }
}
