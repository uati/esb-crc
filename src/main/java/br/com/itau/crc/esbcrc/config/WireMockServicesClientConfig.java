package br.com.itau.crc.esbcrc.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

//@Configuration
public class WireMockServicesClientConfig {

    WireMockServer wireMockServer;

    //@PostConstruct
    public void setUp() {
        wireMockServer = new WireMockServer(7000);
        wireMockServer.start();
        setupStub();
    }

    public void setupStub() {
        wireMockServer.stubFor(post(urlEqualTo("/contratos"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("contratos.json")));
    }


}
