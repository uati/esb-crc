package br.com.itau.crc.esbcrc.v1.contrato.validation;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import lombok.extern.java.Log;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Objects;

@Log
@Component
public class ContratoValidator implements Predicate {

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public boolean matches(Exchange exchange) {
        Contrato contrato = null;
        String str = exchange.getMessage().getBody(String.class);
        try {
            contrato = objectMapper.readValue(str, Contrato.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info(contrato.toString());
        return this.isValid(contrato);
    }

    private boolean isValid(@Valid Contrato contrato) {
        // TODO
        try {
            Preconditions.checkArgument(Objects.nonNull(contrato.getNumero()), "Numero contrato não pode ser null.");
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}