package br.com.itau.crc.esbcrc.v1.contrato.domain;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Produto {

    @NotEmpty
    private String nome;
}
