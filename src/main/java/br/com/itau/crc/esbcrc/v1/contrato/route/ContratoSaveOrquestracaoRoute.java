package br.com.itau.crc.esbcrc.v1.contrato.route;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import lombok.extern.java.Log;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Log
@Component
public class ContratoSaveOrquestracaoRoute extends RouteBuilder {

    public static final String DIRECT_ROUTE_ORQUESTACAO_SAVE = "direct:ROUTE_ORQUESTACAO_SAVE";
    private static final String ID_ORQUESTR = "ID_ORQUESTR";

    @Override
    public void configure() throws Exception {
        from(DIRECT_ROUTE_ORQUESTACAO_SAVE)
                .routeId(ID_ORQUESTR)
                .process(exchange -> {
                    Contrato body = exchange.getIn().getBody(Contrato.class);
                    body.setNumero(body.getNumero()+"===AAA");
                    body.setId(String.valueOf(Math.random()*5));
                    exchange.getOut().setBody(body);
                })
                .setHeader(Exchange.HTTP_RESPONSE_CODE, simple("${in.header.CamelHttpResponseCode}"))
                .endRest();
    }
}
