package br.com.itau.crc.esbcrc.v1.contrato.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contrato {

    private String id;

    @NotNull
    @NotEmpty
    @Size(min = 1)
    private String numero;

    @NotNull
    @Size(min=14, max = 14)
    private String cpfCnpj;

    private String test;

    private int qtdeItens;

    @Valid
    @NotNull
    @Size(min=1)
    @JsonProperty(value = "itens")
    private List<Produto> itens;

}
