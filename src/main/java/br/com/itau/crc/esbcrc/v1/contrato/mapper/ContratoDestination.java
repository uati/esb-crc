package br.com.itau.crc.esbcrc.v1.contrato.mapper;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ContratoDestination {


    private String code;

    private String numero;

    private String cpfCnpj;

    private String test;

    private int qtdeItens;

    private List<ItemDestination> itensDestination;

}
