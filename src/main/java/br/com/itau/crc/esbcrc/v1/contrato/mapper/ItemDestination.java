package br.com.itau.crc.esbcrc.v1.contrato.mapper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemDestination {

    private String name;
}
