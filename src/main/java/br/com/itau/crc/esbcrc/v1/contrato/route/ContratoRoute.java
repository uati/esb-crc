package br.com.itau.crc.esbcrc.v1.contrato.route;

import br.com.itau.crc.esbcrc.v1.contrato.processor.ContratoProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContratoRoute extends RouteBuilder {

    @Autowired
    private ContratoProcessor contratoProcessor;


    @Override
    public void configure() throws Exception {
        from("direct:ROUTE_GET_CONTRATOS")
                .routeId("ID_ROUTE_GET_CTRS")
            .doTry()
                .process(contratoProcessor)
                .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.GET))
                .to("http4://localhost:3000/contratos/")
                .convertBodyTo(String.class)
//                .log("${body}")
            .endDoTry()
            .doFinally()
        .setHeader(Exchange.HTTP_RESPONSE_CODE, simple("${in.header.CamelHttpResponseCode}"));

    }
}
