package br.com.itau.crc.esbcrc.v1.contrato.rest;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ContratoRestRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        rest("contratos")
                .get()
                .consumes("application/json")
                .produces("application/json")
                .route()
                .from("direct:REST_GET_CONTRATOS")
                .routeId("ID_REST_GET_CTRS")
                .to("direct:ROUTE_GET_CONTRATOS")
                .endRest();
    }
}
