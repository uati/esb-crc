package br.com.itau.crc.esbcrc.v1.contrato.rest;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import br.com.itau.crc.esbcrc.v1.contrato.processor.ContratoServiceClientProcessor;
import br.com.itau.crc.esbcrc.v1.contrato.validation.ContratoValidator;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContratoSaveRestRoute extends RouteBuilder {

    public static final String CONTRATOS_URL = "contratos";
    private static final String APPLICATION_JSOM = "application/json";
    private static final String DIRECT_REST_POST_CONTRATOS = "direct:REST_POST_CONTRATOS";
    private static final String ID_REST_POST_CONTRATOS = "ID_REST_POST_CTRS";

    @Autowired
    private ContratoServiceClientProcessor contratoProcessor;

    @Autowired
    private ContratoValidator contratoValidate;

    @Override
    public void configure() throws Exception {

        rest(CONTRATOS_URL)
                .post()
                .consumes(APPLICATION_JSOM)
                .produces(APPLICATION_JSOM)
                .type(Contrato.class)
                .route()
                .from(DIRECT_REST_POST_CONTRATOS)
                .routeId(ID_REST_POST_CONTRATOS)
                .doTry()
                    .validate(contratoValidate)
                    .to("mock:x")
                .endDoTry()
                .endRest();
    }
}
