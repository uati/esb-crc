package br.com.itau.crc.esbcrc.v1.contrato.mapper;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface ContratoMapper {

    ContratoMapper INSTANCE = Mappers.getMapper(ContratoMapper.class);

    @Mappings({
        @Mapping(target = "code", source = "id"),
        @Mapping(target="itensDestination", source = "itens"    )})
    ContratoDestination toDestination(Contrato contrato);

    @AfterMapping
    default void setItens(@MappingTarget ContratoDestination destination, Contrato contrato) {
        contrato.getItens().forEach(p -> destination.getItensDestination().get(contrato.getItens().indexOf(p)).setName(p.getNome()) );
    }


}
