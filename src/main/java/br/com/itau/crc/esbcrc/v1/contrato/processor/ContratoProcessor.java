package br.com.itau.crc.esbcrc.v1.contrato.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class ContratoProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
//        exchange.getOut().setBody(customer);
        exchange.getOut().setBody("");
        exchange.getOut().removeHeader("CamelHttpUri");
        exchange.getOut().removeHeader("CamelHttpUrl");
        exchange.getOut().removeHeader("CamelHttpPath");
        exchange.getOut().setHeader(Exchange.HTTP_METHOD, "GET");


    }
}
