package br.com.itau.crc.esbcrc.v1.contrato.rest;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import br.com.itau.crc.esbcrc.v1.contrato.route.ContratoSaveOrquestracaoRoute;
import com.github.tomakehurst.wiremock.common.Json;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.model.ModelCamelContext;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
public class ContratoSaveRestRoute3Tests {

    private ModelCamelContext context;

    //@EndpointInject(context = "context" , uri = ContratoSaveRestRoute2.DIRECT_REST_ROUTE_POST_CONTRATOS)
    private MockEndpoint mockEndpoint;


    private ProducerTemplate producerTemplate;
    private ContratoSaveRestRoute2 contratoSaveRestRoute2;
    private Contrato contrato;

    @Before
    public void setUp() throws Exception {

        contrato = Contrato.builder().cpfCnpj("12352565232587").numero("1").qtdeItens(2).build();
        
        context = new DefaultCamelContext();
        producerTemplate = context.createProducerTemplate();
        mockEndpoint = MockEndpoint.resolve(context, "mock:"+ContratoSaveRestRoute2.DIRECT_REST_ROUTE_POST_CONTRATOS);
        contratoSaveRestRoute2 = new ContratoSaveRestRoute2();

        context.addRoutes(contratoSaveRestRoute2);
        context.start();

        context.getRouteDefinition(ContratoSaveRestRoute2.ID_REST_ROUTE_POST_CONTRATOS).adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() throws Exception {
                interceptSendToEndpoint(ContratoSaveOrquestracaoRoute.DIRECT_ROUTE_ORQUESTACAO_SAVE).skipSendToOriginalEndpoint().process(ex -> {
                    ex.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_OK);
                    ex.getOut().setBody(contrato);
                    //ex.setProperty(ContratoSaveRestRoute2.DIRECT_REST_ROUTE_POST_CONTRATOS,"REST_ROUTE_POST_CONTRATOS");
                })
                .end();

            }
        });

    }

    @After
    public void finish() throws Exception {
        context.stop();
        producerTemplate.cleanUp();
        mockEndpoint.reset();
    }


    @Test
    public void testMoveFileMock() throws InterruptedException {

        
        String write = Json.write(contrato);

        Assert.assertNotNull(write);

        Exchange exchange = new DefaultExchange(context);
        exchange.getIn().setBody(contrato);
//        //Mockito.when(contratoSaveRestRoute2.ma)
        producerTemplate.send(ContratoSaveRestRoute2.DIRECT_REST_ROUTE_POST_CONTRATOS, exchange);
        Assert.assertNotNull(mockEndpoint.getExchanges());
        //Assert.assertTrue(mockEndpoint.getExchanges().size() > 0);


//        System.out.println(mockEndpoint.getExchanges());
//        Exchange exchange1 = mockEndpoint.getExchanges().get(0);

    }


}
