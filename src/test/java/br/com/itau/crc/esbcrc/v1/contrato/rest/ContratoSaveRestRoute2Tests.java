package br.com.itau.crc.esbcrc.v1.contrato.rest;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import com.github.tomakehurst.wiremock.common.Json;
import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest
@MockEndpoints
public class ContratoSaveRestRoute2Tests {

    @Autowired
    CamelContext context;

    @Autowired
    Environment environment;

    @EndpointInject(uri = ContratoSaveRestRoute2.DIRECT_REST_ROUTE_POST_CONTRATOS)
    private MockEndpoint mockCamel;

    @Autowired
    protected CamelContext createCamelContext() {

        return context;
    }

    @Autowired
    ProducerTemplate producerTemplate;


    @Test
    public void testMoveFileMock() throws InterruptedException {

        //String message = "type,sku#,itemdescription,price\n" +
        //        "ADD,100,Samsung TV,500\n" +
        //        "ADD,101,LG TV,500";

        Contrato contrato = Contrato.builder().cpfCnpj("12352565232587").numero("1").qtdeItens(2).build();
        String write = Json.write(contrato);

        mockCamel.expectedMessageCount(1);
        mockCamel.expectedBodiesReceived(write);

        //producerTemplate.sendBodyAndHeader(environment.getProperty("startRoute")
        //        ,write,"env",environment.getProperty("spring.profiles.active"));

        mockCamel.assertIsSatisfied();

    }


}
