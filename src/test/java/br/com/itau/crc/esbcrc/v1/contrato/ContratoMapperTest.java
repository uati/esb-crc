package br.com.itau.crc.esbcrc.v1.contrato;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import br.com.itau.crc.esbcrc.v1.contrato.domain.Produto;
import br.com.itau.crc.esbcrc.v1.contrato.mapper.ContratoDestination;
import br.com.itau.crc.esbcrc.v1.contrato.mapper.ContratoMapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ContratoMapperTest {

    @Test
    public void givenContratoToDestination_whenMaps_thenCorrect() {

        Contrato resource =
                Contrato.builder()
                        .id("q3rferfvrth")
                        .cpfCnpj("233545")
                        .numero("7777")
                        .qtdeItens(3)
                        .test("test")
                        .itens(Arrays.asList(Produto.builder().nome("TV SAMSUNG").build()))
                        .build();

        ContratoDestination destination = ContratoMapper.INSTANCE.toDestination(resource);

        Assert.assertNotNull(destination.getCode());
        Assert.assertNotNull(destination.getCpfCnpj());
        Assert.assertNotNull(destination.getNumero());
        Assert.assertEquals(destination.getQtdeItens(), resource.getQtdeItens());
        Assert.assertNotNull(destination.getItensDestination());
        Assert.assertTrue(destination.getItensDestination().size() > 0);
        Assert.assertEquals(destination.getItensDestination().get(0).getName(), resource.getItens().get(0).getNome());
    }

}
