package br.com.itau.crc.esbcrc.v1.contrato.IT;

import br.com.itau.crc.esbcrc.Application;
import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import br.com.itau.crc.esbcrc.v1.contrato.rest.ContratoSaveRestRoute;
import br.com.itau.crc.esbcrc.v1.fixtures.templates.ContratoFixtureTemplate;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.github.tomakehurst.wiremock.common.Json;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@ActiveProfiles("integration-test")
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContratoSaveRestRouteIT {

    @LocalServerPort
    private int serverPort;

    private Contrato contrato;

    @BeforeClass
    public static void init() {
        FixtureFactoryLoader.loadTemplates(ContratoFixtureTemplate.class.getPackage().getName());
    }

    @Before
    public void setUp() {
        RestAssured.port = this.serverPort;
        this.contrato = Fixture.from(Contrato.class).gimme(ContratoFixtureTemplate.DOMAIN_CONTRATO_WITHOUT_ID);
    }

    @Test
    public void test_post_should_request_one_contrato_without_id() {
        String write = Json.write(contrato);
        given()
            .contentType(ContentType.JSON)
            .content(write)
            .when()
            .post(ContratoSaveRestRoute.CONTRATOS_URL)
            .then()
            .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void test_post_should_request_one_contrato_without_id_and_numero() {
        Contrato contratoBad = Fixture.from(Contrato.class).gimme(ContratoFixtureTemplate.DOMAIN_CONTRATO_WITHOUT_ID_AND_NUMBER);
        String write = Json.write(contratoBad);
        given()
            .contentType(ContentType.JSON)
            .content(write)
            .when()
            .post(ContratoSaveRestRoute.CONTRATOS_URL)
            .then()
            .statusCode(HttpStatus.SC_BAD_REQUEST);
    }
}
