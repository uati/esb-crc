package br.com.itau.crc.esbcrc.v1.fixtures.templates;

import br.com.itau.crc.esbcrc.v1.contrato.domain.Contrato;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class ContratoFixtureTemplate implements TemplateLoader {

    public static final String DOMAIN_CONTRATO_WITHOUT_ID = "DOMAIN_CONTRATO_WITHOUT_ID";
    public static final String DOMAIN_CONTRATO_WITHOUT_ID_AND_NUMBER = "DOMAIN_CONTRATO_WITHOUT_ID_NUMBER";

    public class ContratoFields {
        public static final String ID = "id";
        public static final String NUMERO = "numero";
        public static final String TEST = "test";
        public static final String QTDE_ITENS = "qtdeItens";
    }


    @Override
    public void load() {
        Fixture.of(Contrato.class).addTemplate(DOMAIN_CONTRATO_WITHOUT_ID, new Rule() {
            {
                add(ContratoFields.ID, null);
                add(ContratoFields.NUMERO, "sdf52dfv5");
                add(ContratoFields.TEST, "test");
                add(ContratoFields.QTDE_ITENS, 5);
            }
        });
        Fixture.of(Contrato.class).addTemplate(DOMAIN_CONTRATO_WITHOUT_ID_AND_NUMBER, new Rule() {
            {
                add(ContratoFields.ID, null);
                add(ContratoFields.NUMERO, null);
                add(ContratoFields.TEST, "test");
                add(ContratoFields.QTDE_ITENS, 5);
            }
        });

    }

}
